
# components
# from utils import QdrantVectorstore
from chain import load_chain


# dependencies
from fastapi import FastAPI, Request, HTTPException
from fastapi.templating import Jinja2Templates
from datetime import datetime
from dotenv import load_dotenv
import openai
import uvicorn
import os


# Initialization
app = FastAPI()
templates = Jinja2Templates(directory="templates/")

# Load environment variables from .env file
load_dotenv()

# Initialize OpenAI API key
openai.api_key = os.getenv('APP_OPENAI_KEY')

# API endpoints


@app.get("/")
async def home(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

# Define endpoint for text completion API


@app.post('/complete')
async def complete_text(request: Request):

    # Retrieve request data
    data = await request.json()

    llm = load_chain()

    output = llm(data["question"])

    return {'answer': output['text']}


# endpoint for testing

@app.get("/time")
def get_current_time():
    return {"time": datetime.now()}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
