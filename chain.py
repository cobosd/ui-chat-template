# from langchain.chains import ChatVectorDBChain
from langchain.chains.chat_vector_db.prompts import (
    CONDENSE_QUESTION_PROMPT, QA_PROMPT)
import os
from langchain import LLMChain, OpenAI, PromptTemplate
from langchain.chains.question_answering import load_qa_chain
from langchain.chains.conversational_retrieval.prompts import CONDENSE_QUESTION_PROMPT
from langchain.chains import ConversationalRetrievalChain #for chatting with data
from dotenv import load_dotenv


def load_chain():
    """Contruct a chat chain to query vectorstore and return the answer. This already includes chat memory and streaming callback"""
    # Load environment variables from .env file
    # Initialize OpenAI API key
    load_dotenv()

    api_key = os.getenv('APP_OPENAI_KEY')
    
    print('\n\napi_key: ', api_key, "\n\n")
    
    prompt_template = "What is a good name for a company that makes {product}?"
    llm = OpenAI(temperature=0, openai_api_key=api_key)
    llm_chain = LLMChain(
    llm=llm,
    prompt=PromptTemplate.from_template(prompt_template)
    )

    return llm_chain
